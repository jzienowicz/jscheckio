/*
From a Array of Integers you have to create a list of closed intervals as Arrays, so the intervals are covering all the values found in the set.

A closed interval includes its endpoints! The interval 1..5, for example, includes each value x that satifies the condition 1 <= x <= 5.

Values can only be in the same interval if the difference between a value and the next smaller value in the set equals one, otherwise a new interval begins. Of course, the start value of an interval is excluded from this rule.
A single value, that does not fit into an existing interval becomes the start- and endpoint of a new interval.
https://js.checkio.org/mission/create-intervals/solve/
*/

"use strict";

function createIntervals(data) {
    if (data.length === 1){
        return [data[0], data[0]];
    }
    data = data.sort(function(a, b){
            if (a > b) {
                return 1;
            }
            if (b > a) {
                return -1;
            }
                return 0;
        });
    console.log(data);
    let result = [];
    let min = data[0];
    for (let index = 1; index < data.length; index++){
        if (data[index] !== data[index - 1] + 1){
            result.push([min, data[index - 1]]);
            min = data[index];
        }
        if (index === data.length - 1){
            result.push([min, data[index]]);
        }
    }
    console.log(result);
    return result;
}

var assert = require('assert');

if (!global.is_checking) {
    assert.deepEqual(createIntervals([1]), [[1, 8]], "Second")
    assert.deepEqual(createIntervals([1, 2, 3, 4, 5, 7, 8, 12]), [[1, 5], [7, 8], [12, 12]], "First")
    console.log("Coding complete? Click 'Check' to review your tests and earn cool rewards!");
}