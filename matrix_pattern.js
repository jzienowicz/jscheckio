/*
To explore new islands and areas Sophia uses the automated drones. But she gets tired looking at the monitors all the time. Sophia wants to teach the drones to recognize some basic patterns and mark them for review.

The drones have a simple optical monochromatic image capturing system. Thanks to this an image can be represented as a binary matrix. You should write a program to search a binary matrix (a pattern) within another binary matrix (an image). The recognition process consists of scanning the image matrix row by row (horizontal scanning) and when a pattern is located on the image, the program must mark this pattern. To mark a located pattern change 1 to 3 and 0 to 2. The result will be the image matrix where the located patterns are marked.

The patterns in the image matrix are not crossed, because you should immediately mark the pattern.
https://js.checkio.org/en/mission/matrix-pattern/
*/

function matrix(pattern, image) {
    for (let image_x = 0; image_x < image.length; image_x++){
        for (let image_y = 0; image_y < image[0].length; image_y++){
            let found_pattern = true;
            for (let pattern_x = 0; pattern_x < pattern.length && found_pattern; pattern_x++){
                for (let pattern_y = 0; pattern_y < pattern[0].length && found_pattern; pattern_y++){
                    if (pattern_x + image_x < image.length && pattern_y + image_y < image[0].length){
                        if (image[image_x + pattern_x][image_y + pattern_y] !== pattern[pattern_x][pattern_y]) {
                            found_pattern = false;
                        }
                    }
                    else {
                        found_pattern = false;
                    }
                }
            }
            if (found_pattern){
                for (let pattern_x = 0; pattern_x < pattern.length; pattern_x++){
                    for (let pattern_y = 0; pattern_y < pattern[0].length; pattern_y++){
                        image[image_x + pattern_x][image_y + pattern_y] += 2;
                    }
                }
            }
        }
    }
    return image;
}

var assert = require('assert');
if (!global.is_checking) {
    // These "asserts" are used for self-checking and not for an auto-testing
    assert.deepEqual(matrix([[1, 0], [1, 1]],
                   [[0, 1, 0, 1, 0],
                    [0, 1, 1, 0, 0],
                    [1, 0, 1, 1, 0],
                    [1, 1, 0, 1, 1],
                    [0, 1, 1, 0, 0]]),   [[0, 3, 2, 1, 0],
                                          [0, 3, 3, 0, 0],
                                          [3, 2, 1, 3, 2],
                                          [3, 3, 0, 3, 3],
                                          [0, 1, 1, 0, 0]])
    assert.deepEqual(matrix([[1, 1], [1, 1]],
                   [[1, 1, 1],
                    [1, 1, 1],
                    [1, 1, 1]]),   [[3, 3, 1],
                                    [3, 3, 1],
                                    [1, 1, 1]])
    assert.deepEqual(matrix([[0, 1, 0], [1, 1, 1]],
                   [[0, 0, 1, 0, 0, 0, 0, 0, 1, 0],
                    [0, 1, 1, 1, 0, 0, 0, 1, 1, 1],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 1, 0, 0, 0, 0],
                    [0, 1, 0, 0, 1, 1, 1, 0, 1, 0],
                    [1, 1, 1, 0, 0, 0, 0, 0, 1, 1],
                    [0, 0, 0, 1, 1, 1, 0, 0, 0, 0],
                    [0, 0, 1, 0, 0, 0, 0, 1, 0, 0],
                    [0, 1, 1, 0, 0, 0, 1, 1, 1, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]),   [[0, 2, 3, 2, 0, 0, 0, 2, 3, 2],
                                                         [0, 3, 3, 3, 0, 0, 0, 3, 3, 3],
                                                         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                                                         [0, 0, 0, 0, 2, 3, 2, 0, 0, 0],
                                                         [2, 3, 2, 0, 3, 3, 3, 0, 1, 0],
                                                         [3, 3, 3, 0, 0, 0, 0, 0, 1, 1],
                                                         [0, 0, 0, 1, 1, 1, 0, 0, 0, 0],
                                                         [0, 0, 1, 0, 0, 0, 2, 3, 2, 0],
                                                         [0, 1, 1, 0, 0, 0, 3, 3, 3, 0],
                                                         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]])
    console.log("Coding complete? Click 'Check' to earn cool rewards!");
}