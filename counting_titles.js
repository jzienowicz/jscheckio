"use strict";
const directions = [[0, 0], [0, 1], [1, 0], [1, 1]];


function countingTiles(radius){
    let side = Math.ceil(radius) * 2;
    let center = side / 2;
    let result = [0, 0];
    for (let x = 0; x < side; x++){
        for (let y = 0; y < side; y++){
            let in_circle = false;
            let out_circle = false;
            for (let corner of directions) {
                if (Math.sqrt(Math.pow(center - (x + corner[0]), 2) + Math.pow(center - (y + corner[1]), 2)) > radius) {
                    out_circle = true;
                } else {
                    in_circle = true;
                }
            }
            if (in_circle && out_circle){
                result[1]++;
            }
            else if(in_circle){
                result[0]++;
            }
        }
    }
    return result;
}

var assert = require('assert');

if (!global.is_checking) {
    assert.deepEqual(countingTiles(2), [4, 12], "N=2");
    assert.deepEqual(countingTiles(3), [16, 20], "N=3");
    assert.deepEqual(countingTiles(2.1), [4, 20], "N=2.1");
    assert.deepEqual(countingTiles(2.5), [12, 20], "N=2.5");
    console.log("Coding complete? Click 'Check' to review your tests and earn cool rewards!");
}
