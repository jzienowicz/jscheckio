/*
Your task is to count the territory that belongs to each player. For this example the answer is: {'B': 13, 'W': 12}.
https://js.checkio.org/en/mission/the-territory-of-go/
*/

function territory(board) {
    for (let x = 0; x < board.length; x++){
        board[x] = board[x].split('');
    }
    let score = {'B': 0, 'W': 0};
    let check_neighbours = [];
        for (let x = 0; x < board.length; x++){
            for (let y = 0; y < board.length; y++){
                let field = 1;
                let owner = ''
                while (check_neighbours.length){
                    for (let up_down = -1; up_down < 2; up_down++){
                        for (let left_right = -1; left_right < 2; left_right++){
                            if (0 < x + up_down && board.length > x + up_down && 0 < y + up_down && board.length > y + up_down) {
                                if (board[x + up_down][y + left_right] === '+') {
                                    field += 1;
                                    board[x + up_down][y + left_right] = '-';
                                    check_neighbours.push([x + up_down, y + left_right]);
                                } else if (board[x + up_down][y + left_right] === 'W') {
                                    console.log(owner);
                                    if (!owner) {
                                        owner = 'W';
                                    } else if (owner !== 'W') {
                                        owner = 'nobody';
                                    }
                                } else if (board[x + up_down][y + left_right] === 'B') {
                                    if (!owner) {
                                        owner = 'B';
                                    } else if (owner !== 'B') {
                                        owner = 'nobody';
                                    }
                                }
                            }
                        }
                    }
                    check_neighbours.shift();
                }
                if (board[x][y] == '+'){
                    board[x][y] = '-';
                    check_neighbours.push([x, y]);
                }
                else if (owner && owner !== 'nobody'){
                    score[owner] += field;
                }
            }
        }
    return score;
}

var assert = require('assert');
if (!global.is_checking) {
    console.log('Example:')
    console.log(territory(['++B++',
                           '+BB++',
                           'BB+++',
                           '+++++',
                           '+++++']))
}