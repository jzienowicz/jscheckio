"use strict";
function boxProbability(marbles, step) {
    let sumW = Array.from(marbles).filter(i => i === 'w');
    let sumB = Array.from(marbles).filter(i => i === 'b');
    if (step === 1){
        return sumW.length / marbles.length;
    }
    else if (sumW.length === marbles.length) {
        sumW[0] = 'b';
        return boxProbability(sumW.join(''), step - 1)
    }
    else if (!sumW.length){
        sumB[0] = 'w';
        return boxProbability(sumB.join(''), step - 1);
    }
    else {
        sumW[0] = 'b';
        let a = (sumW.length / marbles.length) * boxProbability(sumW.join('') + sumB.join(''), step - 1);
        sumB[0] = 'w';
        sumW[0] = 'w';
        let b = (sumB.length / marbles.length) * boxProbability( sumW.join('') + sumB.join(''), step - 1);
        return a + b;
    }
}

var assert = require('assert');

if (!global.is_checking) {
    console.log(boxProbability('bbw', 3), 0.48, "First");
    console.log(boxProbability('wwb', 3), 0.52, "Second");
    console.log(boxProbability('www', 3), 0.56, "Third");
    console.log(boxProbability('bbbb', 1), 0, "Fifth");
    console.log(boxProbability('wwbb', 4), 0.5, "Sixth");
    console.log(boxProbability('bwbwbwb', 5), 0.48, "Seventh");
    console.log("Coding complete? Click 'Check' to review your tests and earn cool rewards!");
}
