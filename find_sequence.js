/*
You are given a matrix of NxN (4≤N≤10). You should check if there is a sequence of 4 or more matching digits. The sequence may be positioned horizontally, vertically or diagonally (NW-SE or NE-SW diagonals).

https://js.checkio.org/en/mission/find-sequence/
*/
function dire(){
    let temp_direction = [];
    for (let x of [-1, 0, 1]){
        for (let y of [-1, 0, 1]){
            if (x || y){
                temp_direction.push([x, y]);
            }
        }
    }
    return temp_direction
}
const direction = dire();


function sequence(matrix) {
    for (let x = 0; x < matrix.length; x++){
        for (let y = 0; y < matrix.length; y++){
            for (let temp_direction of direction){
                let found = true;
                for (let counter = 1; counter < 4 && found; counter++){
                    if (x + counter * temp_direction[0] < matrix.length && x + counter * temp_direction[0] >= 0 && y + counter * temp_direction[1] < matrix.length && y + counter * temp_direction[1] >= 0){
                        if (matrix[x][y] !== matrix[x + counter * temp_direction[0]][y + counter * temp_direction[1]]) {
                            found = false;
                        }
                    }
                    else {
                        found = false;
                    }
                }
                if (found) {
                    return true;
                }
            }
        }
    }
    return false;
}

var assert = require('assert');
if (!global.is_checking) {
    // These "asserts" are used for self-checking and not for an auto-testing

    assert.equal(sequence([
        [1, 2, 1, 1],
        [1, 1, 4, 1],
        [1, 3, 1, 6],
        [1, 7, 2, 5]
    ]), true)
    assert.equal(sequence([
        [7, 1, 4, 1],
        [1, 2, 5, 2],
        [3, 4, 1, 3],
        [1, 1, 8, 1]
    ]), false)
    assert.equal(sequence([
        [2, 1, 1, 6, 1],
        [1, 3, 2, 1, 1],
        [4, 1, 1, 3, 1],
        [5, 5, 5, 5, 5],
        [1, 1, 3, 1, 1]
    ]), true)
    assert.equal(sequence([
        [7, 1, 1, 8, 1, 1],
        [1, 1, 7, 3, 1, 5],
        [2, 3, 1, 2, 5, 1],
        [1, 1, 1, 5, 1, 4],
        [4, 6, 5, 1, 3, 1],
        [1, 1, 9, 1, 2, 1]
    ]), true)

    console.log("Coding complete? Click 'Check' to earn cool rewards!");
}