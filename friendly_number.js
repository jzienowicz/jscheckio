function friendlyNumber(number, options){
    if (!options) {
        options = {};
    }
    if (!options.base) {
        options.base = 1000;
    }
    if (!options.decimals) {
        options.decimals = 0;
    }
    if (!options.suffix) {
        options.suffix = '';
    }
    if (!options.powers) {
        options.powers = ['', 'k', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y'];
    }
    options.number = number;
    let power;
    for (power = 0; power < options.powers.length; power++){
        if (options.number <= options.base){
            break;
        }
        options.number = options.number / options.base;
    }
    if (options.decimals){
        return String(Math.round(options.number * Math.pow(10, options.decimals)) / Math.pow(10, options.decimals)) + options.suffix + options.powers[power]
    }
    return String((Math.floor(options.number * Math.pow(10, options.decimals)) / Math.pow(10, options.decimals)).toFixed(options.decimals)) + options.suffix + options.powers[power]
}

var assert = require('assert');

if (!global.is_checking) {
    console.log(friendlyNumber(12100000,{"decimals":3}));
    console.log(friendlyNumber(10240));
    console.log(friendlyNumber(12341234, {decimals: 1}));
    console.log(friendlyNumber(12461, {decimals: 1}));
    console.log(friendlyNumber(1024000000, {base: 1024, suffix: 'iB'}));
    console.log("Tests and earn cool rewards!");
}