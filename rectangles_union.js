"use strict";

function rectanglesUnion(recs) {
    let field = 0;
    let maxX = 0;
    let maxY = 0;
    for (let rectangle of recs){
        if (maxX < rectangle[2]){
            maxX = rectangle[2];
        }
        if (maxY < rectangle[3]){
            maxY = rectangle[3];
        }
    }
    for (let x = 0; x < maxX; x++){
        for (let y = 0; y < maxY; y++){
            for (let rectangle of recs){
                if (x >= rectangle[0] && x < rectangle[2] && y >= rectangle[1] && y < rectangle[3]){
                    field++;
                    break;
                }
            }
        }
    }
    return field;
}

var assert = require('assert');
if (!global.is_checking) {
    console.log('Example:')
    console.log(rectanglesUnion([
        [6, 3, 8, 10],
        [4, 8, 11, 10],
        [16, 8, 19, 11]
    ]))

    // These "asserts" are used for self-checking and not for an auto-testing
    assert.equal(rectanglesUnion([
        [6, 3, 8, 10],
        [4, 8, 11, 10],
        [16, 8, 19, 11]
    ]), 33)
    assert.equal(rectanglesUnion([
        [16, 8, 19, 11]
    ]), 9)
    assert.equal(rectanglesUnion([
        [16, 8, 19, 11],
        [16, 8, 19, 11]
    ]), 9)
    assert.equal(rectanglesUnion([
        [16, 8, 16, 8]
    ]), 0)
    assert.equal(rectanglesUnion([

    ]), 0)
    console.log("Coding complete? Click 'Check' to earn cool rewards!");
}