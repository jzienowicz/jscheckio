/*
Your task is to write a function which takes the tuple of tuples containing fractions as an argument and returns the sum of those fractions. The fractions will look like this: (x, y), where 'x' is the numerator, and 'y' is the denominator. For example, (2, 3) means 2/3. If the numerator is greater than the denominator (after the addition) you should extract the integer part and put it before the fraction. For example:
fractions (((2, 3), (2, 3))) = "1 and 1/3", because the result will be - 4/3 (the numerator is greater than the denominator) and you can extract the integer part (1) and the remaining fraction (1/3). Make note that the conjunction 'and' is required if the result has both parts - the integer and the fraction.
If the result doesn’t contain the fraction part and has only the integer - you should return it as the 'int'-type, not 'str'. If it doesn’t contain the integer part - just return it like a string 'N/D' where N - is the numerator and D - is the denominator.
https://js.checkio.org/en/mission/fractions-addition/
*/

function addFractions(fracts) {
    let counter = fracts[0][0];
    let denominator = fracts[0][1];
    for (let x = 1; x < fracts.length; x++){
        if (denominator === fracts[x][1]){
            counter += fracts[x][0];
        }
        else{
            counter = counter * fracts[x][1] + fracts[x][0] * denominator;
            denominator *= fracts[x][1];
        }
    }
    return !(counter % denominator) ? counter / denominator :
                    counter < denominator ? counter + '/' + denominator :
                    Math.floor(counter / denominator) + ' and ' + counter % denominator + '/' + denominator;
}

var assert = require('assert');
if (!global.is_checking) {
    // These "asserts" are used for self-checking and not for an auto-testing
    console.log(addFractions([[2, 3], [2, 3]]))
    console.log(addFractions([[1, 3], [1, 3]]))
    console.log(addFractions([[1, 3], [1, 3], [1, 3]]))
}