/*
You are given a string and two markers (the initial and final). You have to find a substring enclosed between these two markers. But there are a few important conditions:

The initial and final markers are always different.
If there is no initial marker, then the first character should be considered the beginning of a string.
If there is no final marker, then the last character should be considered the ending of a string.
If the initial and final markers are missing then simply return the whole string.
If the final marker comes before the initial marker, then return an empty string.
https://js.checkio.org/en/mission/between-markers/
*/

function betweenMarkers(text, begin, end) {
    let begin2 = text.indexOf(begin);
    let end2 = text.indexOf(end);
    if (begin2 === -1 && end2 === -1) {
        return text;
    }
    else if (begin2 === -1) {
        return text.slice(0, end2);
    }
    else if (end2 === -1) {
        return text.slice(begin2 + begin.length, text.length);
    }
    else if (begin2 < end2) {
        return text.slice(begin2 + begin.length, end2);
    }
    else {
        return '';
    }
}

var assert = require('assert');

if (!global.is_checking) {
    console.log('Example:')
    console.log(betweenMarkers('What is >apple<', '>', '<'), 'apple')

    assert.equal(betweenMarkers('What is >apple<', '>', '<'), 'apple')
    assert.equal(betweenMarkers("<head><title>My new site</title></head>",
                                "<title>", "</title>"), 'My new site')
    assert.equal(betweenMarkers('No[/b] hi', '[b]', '[/b]'), 'No')
    assert.equal(betweenMarkers('No [b]hi', '[b]', '[/b]'), 'hi')
    assert.equal(betweenMarkers('No hi', '[b]', '[/b]'), 'No hi')
    assert.equal(betweenMarkers('No <hi>', '>', '<'), '')
    console.log("Coding complete? Click 'Check' to review your tests and earn cool rewards!");
}
