/*

"Nikola, A1213pokl, bAse730onE."

"Yes Sofia, what is it?"

"You tell me! Your torture device is singing my circuits with its new lexicon. There is no way I can remember these new passwords and the thing doesn't accept simple and easy ones!"

"Oh, those will be good passwords, you can use them."

"Why can’t you use them!?" Sofia asked almost hysterically. "I’ve already forgotten them! Do you want me locked out of my own house for eternity? Come up with something easier so I don’t have to keep all that randomized gobbledygook in my head." Sofia rarely acted so demanding but she had reached her boiling point. The stress of the past few days coupled with her exhaustion had brought her to the edge.

"Don’t worry, I was expecting something easier to remember. Why don’t you use the cipher map to help with your password. With it we can encrypt all the passwords and leave them right next to the door or in a special place that we all agree upon. You will only be able to read them with the use of the cipher map which we will take with us on our trip.

"So I don’t need to remember all of those passwords? All we have to do is make sure that we don't lose the cipher map." asked Sofia with hopefully.

"Yes, that is correct."

"Awesome. Show me the cipher map and explain how this works one more time."

Help Sofia write a decrypter for the passwords that Nikola will encrypt through the cipher map. A cipher grille is a 4×4 square of paper with four windows cut out. Placing the grille on a paper sheet of the same size, the encoder writes down the first four symbols of his password inside the windows (see fig. below). After that, the encoder turns the grille 90 degrees clockwise. The symbols written earlier become hidden under the grille and clean paper appears inside the windows. The encoder then writes down the next four symbols of the password in the windows and turns the grille 90 degrees again. Then, they write down the following four symbols and turns the grille once more. Lastly, they write down the final four symbols of the password. Without the same cipher grille, it is difficult to discern the password from the resulting square comprised of 16 symbols. Thus, the encoder can be confident that no hooligan will easily gain access to the locked door.
https://js.checkio.org/en/mission/cipher-map/
*/

const deepCopyFunction = inObject => {
  let outObject, value, key

  if(typeof inObject !== "object" || inObject === null) {
    return inObject // Return the value if inObject is not an object
  }

  // Create an array or object to hold the values
  outObject = Array.isArray(inObject) ? [] : {}

  for (key in inObject) {
    value = inObject[key]

    // Recursively (deep) copy for nested objects, including arrays
    outObject[key] = (typeof value === "object" && value !== null) ? deepCopyFunction(value) : value
  }

  return outObject
}

function recallPassword(grille, password){
    for (let x = 0; x < grille.length; x++){
        grille[x] = grille[x].split('');
    }
    let new_grille = deepCopyFunction(grille);
    let result = '';
    for (let four_times = 0; four_times < 4; four_times++){
        for (let x = 0; x < grille.length; x++){
            for (let y = 0; y < grille.length; y++){
                if (new_grille[x][y] === 'X'){
                    result += password[x][y];
                }
            }
        }
        for (let y = 0; y < grille.length; y++){
            for (let x = grille.length - 1; x >= 0; x--){
                new_grille[y][grille.length - 1 - x] =  grille[x][y];
            }
        }
        grille = deepCopyFunction(new_grille);
    }
    return result;
}

var assert = require('assert');

if (!global.is_checking) {
    assert.equal(recallPassword(
        ['X...',
         '..X.',
         'X..X',
         '....'],
        ['itdf',
         'gdce',
         'aton',
         'qrdi']), 'icantforgetiddqd', "First Example");
    assert.equal(recallPassword(['....',
         'X..X',
         '.X..',
         '...X'],
        ['xhwc',
         'rsqx',
         'xqzz',
         'fyzr']), 'rxqrwsfzxqxzhczy', "Second Example");
    console.log("Coding complete? Click 'Check' to review your tests and earn cool rewards!");
}
