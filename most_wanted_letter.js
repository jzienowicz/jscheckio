/*
You are given a text, which contains different english letters and punctuation symbols. You should find the most frequent letter in the text. The letter returned must be in lower case.
While checking for the most wanted letter, casing does not matter, so for the purpose of your search, "A" == "a". Make sure you do not count punctuation symbols, digits and whitespaces, only letters.

If you have two or more letters with the same frequency, then return the letter which comes first in the latin alphabet. For example -- "one" contains "o", "n", "e" only once for each, thus we choose "e".
https://js.checkio.org/en/mission/most-wanted-letter/
*/

function mostWanted(data) {
    let dict = [];
    data = data.replace(/[^a-zA-Z_]/gi, '');
    while (data.length) {
        let letter = data.charAt(0).toLowerCase()
        data = data.slice(1)
        if (letter in dict){
            dict[letter]++;
        }
        else {
            dict[letter] = 1;
        }
    }
    let best = ['a', 0]
    for (let x of Object.keys(dict)){
        if ((best[1] < dict[x]) || (best[1] == dict[x] && best[0] > x)) {
            best[0] = x;
            best[1] = dict[x];
        }
    }
    return best[0];
}

var assert = require('assert');

if (!global.is_checking) {
    assert.equal(mostWanted("Hello World!"), "l", "1st example");
    assert.equal(mostWanted("How do you do?"), "o", "2nd example");
    assert.equal(mostWanted("One"), "e", "3rd example");
    assert.equal(mostWanted("Oops!"), "o", "4th example");
    assert.equal(mostWanted("AAaooo!!!!"), "a", "Letters");
    console.log("Coding complete? Click 'Check' to review your tests and earn cool rewards!");
}