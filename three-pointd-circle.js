/*
You should find the circle for the three given points, such that the circle lies through these point and return the result as a string with the equation of the circle. In a Cartesian coordinate system (with an X and Y axis), the circle with central coordinates of (x0,y0) and radius of r can be described with the following equation:

    "(x-x0)^2+(y-y0)^2=r^2"


https://js.checkio.org/mission/three-points-circle/solve/
*/

function calculateDet3x3(matrix){
    return matrix[0][0] * (matrix[1][1] * matrix[2][2] - matrix[1][2] * matrix[2][1])
           - matrix[0][1] * (matrix[1][0] * matrix[2][2] - matrix[2][0] * matrix[1][2])
           + matrix[0][2] * (matrix[1][0] * matrix[2][1] - matrix[2][0] * matrix[1][1]);
}


function circle(data) {
    //#TODO get data to table
    data = data.split(/(?:,|\(|\))+/).filter(function notEmpty(str){
        return str != '';
    });
    data = [[data[0], data[1]], [data[2], data[3]], [data[4], data[5]]];
    let matrix11 = [[data[0][0], data[0][1], 1],
                    [data[1][0], data[1][1], 1],
                    [data[2][0], data[2][1], 1]];
    let matrix12 = [[Math.pow(data[0][0], 2) + Math.pow(data[0][1], 2), data[0][1], 1],
                    [Math.pow(data[1][0], 2) + Math.pow(data[1][1], 2), data[1][1], 1],
                    [Math.pow(data[2][0], 2) + Math.pow(data[2][1], 2), data[2][1], 1]];
    let matrix13 = [[Math.pow(data[0][0], 2) + Math.pow(data[0][1], 2), data[0][0], 1],
                    [Math.pow(data[1][0], 2) + Math.pow(data[1][1], 2), data[1][0], 1],
                    [Math.pow(data[2][0], 2) + Math.pow(data[2][1], 2), data[2][0], 1]];
    let matrix14 = [[Math.pow(data[0][0], 2) + Math.pow(data[0][1], 2), data[0][0], data[0][1]],
                    [Math.pow(data[1][0], 2) + Math.pow(data[1][1], 2), data[1][0], data[1][1]],
                    [Math.pow(data[2][0], 2) + Math.pow(data[2][1], 2), data[2][0], data[2][1]]];
    let x = calculateDet3x3(matrix12) / calculateDet3x3(matrix11) * 0.5;
    let y = - calculateDet3x3(matrix13) / calculateDet3x3(matrix11) * 0.5;
    let r = Math.sqrt(x * x + y * y + calculateDet3x3(matrix14) / calculateDet3x3(matrix11));
    return `(x-${x})^2+(y-${y})^2=${r.toFixed(2)}^2`;
}

var assert = require('assert');
if (!global.is_checking) {
    // These "asserts" are used for self-checking and not for an auto-testing
    assert.equal(circle("(2,2),(6,2),(2,6)"), "(x-4)^2+(y-4)^2=2.83^2")
    assert.equal(circle("(3,7),(6,9),(9,7)"), "(x-6)^2+(y-5.75)^2=3.25^2")
    console.log("Coding complete? Click 'Check' to earn cool rewards!");
}