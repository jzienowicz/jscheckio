/*
You have a sequence of strings, and you’d like to determine the most frequently occurring string in the sequence. It can be only one.
https://js.checkio.org/mission/the-most-frequent/solve/
*/

function mostFrequent(data) {
    let dict = {};
    let biggest = {'': 0};
    for (let word of data){
        word in dict ? dict[word] += 1 : dict[word] = 1;
        if (dict[word] > biggest[Object.keys(biggest)[0]]){
            delete biggest[Object.keys(biggest)[0]];
            biggest[word] = dict[word];
        }
    }
    return Object.keys(biggest)[0];
}

var assert = require('assert');

if (!global.is_checking) {
    console.log('Example:')
    console.log(mostFrequent([
        'a', 'b', 'c',
        'a', 'b',
        'a'
    ]))

    assert.equal(mostFrequent([
        'a', 'b', 'c',
        'a', 'b',
        'a'
    ]), 'a')
    assert.equal(mostFrequent(['a', 'a', 'bi', 'bi', 'bi']), 'bi')
    console.log("Coding complete? Click 'Check' to review your tests and earn cool rewards!");
}